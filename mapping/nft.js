/* eslint-disable max-len */
const services = require("../services");

/**
 * @dev handleNewNFTMinted
 * @param {String} eventName
 * @param {String} address
 * @param {String} blockHash
 * @param {String} blockNumber
 * @param {String} transactionHash
 * @param {Object} values
 */
const handleNewNFTMinted = (eventName, address, blockHash, blockNumber, transactionHash, values) => {
  const {owner, id, name, uri, level} = values;

  services.createTransaction({
    name: eventName,
    blockNumber,
    blockHash,
    transactionHash,
    address,
  });
  services.createNft({
    tokenId: id,
    name,
    uri,
    level,
    power: 0,
    owner,
  });
};

/**
 * @dev handleGetWeapon
 * @param {String} eventName
 * @param {String} address
 * @param {String} blockHash
 * @param {String} blockNumber
 * @param {String} transactionHash
 * @param {Object} values
 */
const handleGetWeapon = (eventName, address, blockHash, blockNumber, transactionHash, values) => {
  const {nftId, nftOwner, power} = values;

  services.createTransaction({
    name: eventName,
    blockNumber,
    blockHash,
    transactionHash,
    address,
  });
  services.updateNft({
    tokenId: nftId,
    owner: nftOwner,
    power,
  });
};

/**
 * @dev handleTransfer
 * @param {String} eventName
 * @param {String} address
 * @param {String} blockHash
 * @param {String} blockNumber
 * @param {String} transactionHash
 * @param {Object} values
 */
const handleTransfer = (eventName, address, blockHash, blockNumber, transactionHash, values) => {
  const {to, tokenId} = values;

  services.createTransaction({
    name: eventName,
    blockNumber,
    blockHash,
    transactionHash,
    address,
  });
  services.updateNft({
    tokenId,
    owner: to,
  });
};

/**
 * @dev handleApproval
 * @param {String} eventName
 * @param {String} address
 * @param {String} blockHash
 * @param {String} blockNumber
 * @param {String} transactionHash
 * @param {Object} values
 */
const handleApproval = (eventName, address, blockHash, blockNumber, transactionHash, values) => {
  // const {owner, approved, tokenId} = values;
  services.createTransaction({
    name: eventName,
    blockNumber,
    blockHash,
    transactionHash,
    address,
  });
};

/**
 * @dev handleApprovalForAll
 * @param {String} eventName
 * @param {String} address
 * @param {String} blockHash
 * @param {String} blockNumber
 * @param {String} transactionHash
 * @param {Object} values
 */
const handleApprovalForAll = (eventName, address, blockHash, blockNumber, transactionHash, values) => {
  // const {owner, operator, approved} = values;
  services.createTransaction({
    name: eventName,
    blockNumber,
    blockHash,
    transactionHash,
    address,
  });
};

module.exports = {
  handleNewNFTMinted,
  handleGetWeapon,
  handleTransfer,
  handleApproval,
  handleApprovalForAll,
};
