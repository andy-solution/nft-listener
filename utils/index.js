const math = require("./math");
const constants = require("./constants");
const generator = require("./generator");

module.exports = {
  ...math,
  ...constants,
  ...generator,
};
