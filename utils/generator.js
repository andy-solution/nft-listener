const mappingEventsToCallback = (event, callback) => {
  const {
    event: name,
    address,
    blockHash,
    blockNumber,
    transactionHash,
    returnValues: values,
  } = event;

  callback(name, address, blockHash, blockNumber, transactionHash, values);
};

module.exports = {
  mappingEventsToCallback,
};
