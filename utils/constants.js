const INFURA_PROJECT_ID = process.env.INFURA_PROJECT_ID;

const RPC_URLS = {
  1: `wss://mainnet.infura.io/ws/v3/${INFURA_PROJECT_ID}`,
  4: `wss://rinkeby.infura.io/ws/v3/${INFURA_PROJECT_ID}`,
  42: `wss://kovan.infura.io/ws/v3/${INFURA_PROJECT_ID}`,
  56: "https://bsc-dataseed1.binance.org",
  97: "https://data-seed-prebsc-1-s1.binance.org:8545",
  137: "https://rpc-mainnet.matic.network",
  80001: "https://rpc-mumbai.matic.today",
  128: "https://http-mainnet.hecochain.com",
  256: "https://http-testnet.hecochain.com",
};

module.exports = {
  RPC_URLS,
};
