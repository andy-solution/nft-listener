const {NFTContract, GFXContract} = require("./addresses");

const nftAbi = require("./abi/nft.json");
const gfxAbi = require("./abi/gfx.json");

const getNftContractInstance = async (web3) => {
  console.log("DEBUG-NFTContract:", NFTContract);
  return await (new web3.eth.Contract(nftAbi, NFTContract));
};

const getGfxContractInstance = async (web3) => {
  return await (new web3.eth.Contract(gfxAbi, GFXContract));
};

module.exports = {
  getNftContractInstance,
  getGfxContractInstance,
};
