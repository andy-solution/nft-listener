require("dotenv").config();
const chainId = process.env.CHAIN_ID || 1;
const INFURA_PROJECT_ID = process.env.INFURA_PROJECT_ID;

const RPC_URLS = {
  1: `wss://mainnet.infura.io/ws/v3/${INFURA_PROJECT_ID}`,
  4: `wss://rinkeby.infura.io/ws/v3/${INFURA_PROJECT_ID}`,
  42: `wss://kovan.infura.io/ws/v3/${INFURA_PROJECT_ID}`,
  56: "wss://bsc-dataseed1.binance.org",
  97: "wss://data-seed-prebsc-1-s1.binance.org:8545",
  137: "wss://rpc-mainnet.matic.network",
  80001: "wss://rpc-mumbai.matic.today",
  128: "wss://http-mainnet.hecochain.com",
  256: "wss://http-testnet.hecochain.com",
};

module.exports = {
  rpcUrl: RPC_URLS[chainId],
};
