require("dotenv").config();

const express = require("express");
const run = require("./controllers");

const app = express();
const port = process.env.SERVER_PORT || 3000;

app.listen(port, () => {
  run();
  console.log(`Server is running on ${port} port`);
});
