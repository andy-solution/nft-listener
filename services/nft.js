const models = require("../models");

const createNft = async (payload) => {
  await models.Nft.create(payload);
};

const updateNft = async (payload) => {
  await models.Nft.update(payload, {
    where: {
      tokenId: payload.tokenId,
      owner: payload.owner,
    },
  });
};

module.exports = {
  createNft,
  updateNft,
};
