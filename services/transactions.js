/* eslint-disable max-len */
const models = require("../models");

const createTransaction = async (payload) => {
  await models.Transaction.create(payload);
};

module.exports = {
  createTransaction,
};
