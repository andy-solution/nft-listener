/* eslint-disable new-cap */
/* eslint-disable max-len */
const nft = require("../mapping/nft");
const {mappingEventsToCallback} = require("../utils");

const addEventListeners = (nftInstance) => {
  if (!nftInstance) return;

  console.log("DEBUG-", nftInstance.events);

  // New NFT Minted
  nftInstance.events.NewNFTMinted()
    .on("connected", (subscriptionId) => console.log("NewNFTMinted Subsription: ", subscriptionId))
    .on("data", (event) => mappingEventsToCallback(event, nft.handleNewNFTMinted))
    .on("error", console.error);

  // Weapon
  nftInstance.events.GetWeapon()
    .on("connected", (subscriptionId) => console.log("GetWeapon Subsription: ", subscriptionId))
    .on("data", (event) => mappingEventsToCallback(event, nft.handleGetWeapon))
    .on("error", console.error);

  // NFT Transfer
  nftInstance.events.Transfer()
    .on("connected", (subscriptionId) => console.log("Transfer Subsription: ", subscriptionId))
    .on("data", (event) => mappingEventsToCallback(event, nft.handleTransfer))
    .on("error", console.error);

  // NFT Transfer approvement
  nftInstance.events.Approval()
    .on("connected", (subscriptionId) => console.log("Approval Subsription: ", subscriptionId))
    .on("data", (event) => mappingEventsToCallback(event, nft.handleApproval))
    .on("error", console.error);

  // NFT approve
  nftInstance.events.ApprovalForAll()
    .on("connected", (subscriptionId) => console.log("ApprovalForAll Subsription: ", subscriptionId))
    .on("data", (event) => mappingEventsToCallback(event, nft.handleApprovalForAll))
    .on("error", console.error);
};

module.exports = {
  addEventListeners,
};
