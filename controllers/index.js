const Web3 = require("web3");

const nft = require("./nft");
const {
  // getGfxContractInstance,
  getNftContractInstance,
} = require("../contracts/instances");
const {rpcUrl} = require("../contracts/rpcUrls");

console.log("DEBUG-RPC_URL", rpcUrl);

const runApp = async () => {
  const web3 = new Web3();
  await web3.setProvider(new Web3.providers.WebsocketProvider(rpcUrl));

  const nftInstance = await getNftContractInstance(web3);
  nft.addEventListeners(nftInstance);
};

module.exports = runApp;
